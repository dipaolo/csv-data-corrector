/*******************************************************************************
* Description:
*    Matrix class provides basic regular functionality of filling
*    the matrix, calculating average, getting slice and
*    perforing correction of "bad" values in the matrix ("bad" values
*    are zero values in such case).
*
* Author:
*    Pavel "DiPaolo" Ditenbir (pavel.ditenbir@gmail.com)
*
* Copyright (c) Pavel Ditenbir, 2017
*
*******************************************************************************/
#pragma once

#include <iostream>
#include <numeric>
#include <stdexcept>
#include <vector>


template <typename T, std::size_t Cols>
class Matrix
{
public:
    typedef std::array<T, Cols> Row;

private:
    typedef Matrix<T, Cols> MatrixType;

public:
    Matrix() {}
    ~Matrix() {};

    bool addRow(const Row &row)
    {
        m_matrix.push_back(row);
        m_rows++;
        return true;
    }

    int cols() const
    {
        return Cols;
    }

    int rows() const
    {
        return static_cast<int>(m_matrix.size());
    }

    int64_t size() const
    {
        return (int64_t)(cols()) * rows();
    }

    T average() const
    {
        using namespace std;

        double sum = 0.0;
        vector<Row>::const_iterator iter = m_matrix.begin();
        for (; iter != m_matrix.end(); ++iter)
            sum += accumulate(iter->begin(), iter->end(), 0);
        
        const auto count = m_matrix.size() * Cols;
        return static_cast<T>(sum / count);
    }

    MatrixType slice(int startRow, int endRow) const
    {
        using namespace std;

        if (startRow < 0 || endRow >= rows())
        {
            throw invalid_argument("Unable to return slice of a matrix");
            return MatrixType();
        }

        MatrixType slice;
        for (int i = startRow; i <= endRow; i++)
            slice.addRow(m_matrix[i]);

        return slice;
    }

    void print(std::ostream &out = std::cout) const
    {
        using namespace std;

        for (auto row : m_matrix)
        {
            for (auto element : row)
                out << element << " ";
            out << endl;
        }
    }


    // As long as we know nothing about the data and its specific,
    // use the simplest method - just replace "bad" values with
    // average values of the cell's neighbors by using filter NxN,
    // where N is column count.
    //
    // Other methods can be added later depending on data specific
    int correctDataWithAverageFilter()
    {
        using namespace std;

        vector<Row>::iterator iter = m_matrix.begin();
        for (int row = 0; iter != m_matrix.end(); ++iter, row++)
        {
            for (auto &element : *iter)
            {
                if (abs(element) <= numeric_limits<T>::epsilon())
                {
                    const auto startRow = max<int>(0, row - Cols / 2);
                    const auto endRow = min<int>(row + Cols / 2, rows() - 1);

                    const MatrixType filterMatrix = slice(startRow, endRow);
                    auto average = filterMatrix.average();
                    const auto size = filterMatrix.size();

                    // exclude the "bad" value from calculation to 
                    // get more valid result
                    element = (int64_t)(average) * size / (size - 1);
                }
            }
        }

        return 0;
    }

private:
    int m_rows = 0;
    std::vector<Row> m_matrix;
};