/*******************************************************************************
* Description:
*    contains main() function. Please use readme.me file to get more
*    information.
*
* Author:
*    Pavel "DiPaolo" Ditenbir (pavel.ditenbir@gmail.com)
*
* Copyright (c) Pavel Ditenbir, 2017
*
*******************************************************************************/
#include <iostream>

#include "csv_utils.h"
#include "matrix.h"

static void PrintHelp();

constexpr int kColsCount = 3;
csv::Delimeters GetCustomDelimeters()
{
    csv::Delimeters delims;
    delims.push_back(' ');
    delims.push_back(',');
    delims.push_back('\n');
    return delims;
}


int main(int argc, char *argv[])
{
    using namespace std;

    constexpr int kRequiredArgsCount = 2;
    if (argc != kRequiredArgsCount + 1)
    {
        cout << "Error: Invalid arguments count" << endl;
        PrintHelp();
        return 1;
    }

    try
    {
        const string inputFilename(argv[1]);
        Matrix<int, kColsCount> matrix = csv::ReadMatrixFromCsvFile<int, kColsCount>(inputFilename, GetCustomDelimeters());

        cout << "Initial data:" << endl;
        matrix.print();

        matrix.correctDataWithAverageFilter();

        cout << endl << "Data after correction:" << endl;
        matrix.print();
        cout << endl;

        const string outputFilename(argv[2]);
        csv::WriteMatrixToCsvFile<int, kColsCount>(matrix, outputFilename);
    }
    catch (exception &ex)
    {
        cout << "Error: " << ex.what() << endl;
        return 1;
    }

    cout << "Successfully done.";
    return 0;
}

void PrintHelp()
{
    using namespace std;
    cout << endl
         << "Usage:" << endl
         << "   csv_data_corrector <input.csv> <output.csv>" << endl;
}
