## About
This is a test tast for NavVis company.

## Task description
Write an application in C++ that reads a CSV file, processes it and writes the output as CSV file back to disk.

To read the input: The CSV file basically contains a 2D matrix of numbers, where each line holds a single row: "2\<delim\>4\<delim\>99\<delim\>\n". The delimiter can be either space or a single comma (�,�). Write the output in the same format as the input.

Once the input data is read, your application should perform filtering of �bad� values. Any entry of the matrix is �bad� when it has a value of 0. The application should now replace these bad values and compute a valid value by interpolating it from its surrounding, i.e., from the spatial neighbors in the matrix. You may assume that no two bad values are adjacent to each other.

Write the matrix with the replaced values to disk.

Your program should accept the input and output path as program <input> <output>.

#### Remarks:
* You may not use any 3rd party code or libraries. You may use the standard headers* and STL. Standard headers are listed here: http://en.cppreference.com/w/

## Algoritm
As long as we know nothing about the data and its specific, use the simplest method - just replace "bad" values with average values of the cell's neighbors by using filter NxN, where N is column count.

Other methods can be added later depending on data specific.

## Build
1. create **build** folder
2. go to that folder
3. generate project file using CMake (CMake 3.1 is minimal requirement):
```
cmake .. -G <your generator>
```
For example, for Microsoft Visual Studio 2017 you may use the following command:
```
cmake .. -G "Visual Studio 15 2017 Win64"
```
4. open generate project file in your favourite IDE or build it with using **make**/**nmake**.