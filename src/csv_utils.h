/*******************************************************************************
* Description:
*    helper functions for reading and writing matrix from/to CSV file.
*
* Author:
*    Pavel "DiPaolo" Ditenbir (pavel.ditenbir@gmail.com)
*
* Copyright (c) Pavel Ditenbir, 2017
*
*******************************************************************************/
#pragma once

#include <array>
#include <fstream>
#include <iostream>
#include <locale>
#include <sstream>
#include <string>

#include "matrix.h"

namespace csv {

typedef std::vector<char> Delimeters;

// creates a locale with required delimeters to use custom delimeters with istream
static std::locale SetupDelimeters(const std::locale &curLocale, const Delimeters &delimeters)
{
    using namespace std;

    struct customCType : ctype<char>
    {
        customCType(const Delimeters &delimeters) : ctype<char>(getTable(delimeters)) {}
        static mask const *getTable(const Delimeters &delimeters)
        {
            static mask delims[table_size];
            for (auto delimeter : delimeters)
                delims[delimeter] = ctype_base::space;
            return &delims[0];
        }
    };

    // we don't nee to free the object later due to http://en.cppreference.com/w/cpp/locale/locale/locale:
    // Overload 7 is typically called with its second argument, f, obtained directly from a $
    // new-expression: the locale is responsible for calling the matching delete from its own destructor. 
    return locale(curLocale, new customCType(delimeters));
}


template<typename T, std::size_t Cols>
Matrix<T, Cols> ReadMatrixFromCsvFile(const std::string &filename, const Delimeters &delimeters = Delimeters());

template<typename T, std::size_t Cols>
Matrix<T, Cols> WriteMatrixToCsvFile(const Matrix<T, Cols> &matrix, const std::string &filename);


template<typename T, std::size_t Cols>
Matrix<T, Cols> ReadMatrixFromCsvFile(const std::string &filename, const Delimeters &delimeters /* = Delimeters() */)
{
    using namespace std;

    ifstream fileStream(filename, ifstream::in);
    if (!fileStream.is_open())
    {
        stringstream ssMsg;
        ssMsg << "Failed to open input CSV file " << filename;
        throw exception(ssMsg.str().c_str());
        return Matrix<T, Cols>();
    }

    locale customLocale = fileStream.getloc();
    if (delimeters.size() > 0)
        customLocale = SetupDelimeters(fileStream.getloc(), delimeters);

    typedef array<T, Cols> LineValues;
    LineValues lineValues;

    // read line by line from file and try to
    // get Cols amount of values of type T at every line
    string line;
    int lineNumber = 1;
    Matrix<T, Cols> matrix;
    while (!fileStream.eof())
    {
        getline(fileStream, line);

        stringstream ss(line);
        ss.imbue(customLocale);

        LineValues::iterator iter = lineValues.begin();
        for (; iter != lineValues.end(); ++iter)
        {
            ss >> *iter;
            if (ss.fail())
            {
                stringstream ssMsg;
                ssMsg << "Failed to parse input CSV file at line " << lineNumber;
                throw exception(ssMsg.str().c_str());
                return Matrix<T, Cols>();
            }
        }

        // check if there is something unexpected left on that line 
        if (!ss.eof())
        {
            T extraValue = 0;
            ss >> extraValue;

            /*
            Examples:
                11 12 13  			eof fail
                21 22 23   1		eof
                31 32 33.3				fail
            */
            if (ss.eof() ^ ss.fail())
            {
                stringstream ssMsg;
                ssMsg << "Invalid SCV format. There is an error at line " << lineNumber;
                throw exception(ssMsg.str().c_str());
                return Matrix<T, Cols>();
            }
        }

        matrix.addRow(lineValues);
        lineNumber++;
    }

    fileStream.close();
    return matrix;
}

template<typename T, std::size_t Cols>
Matrix<T, Cols> WriteMatrixToCsvFile(const Matrix<T, Cols> &matrix, const std::string &filename)
{
    using namespace std;

    ofstream fileStream(filename, ifstream::out);
    if (!fileStream.is_open())
    {
        stringstream ssMsg;
        ssMsg << "Failed to open output CSV file " << filename;
        throw exception(ssMsg.str().c_str());
        return Matrix<T, Cols>();
    }

    matrix.print(fileStream);

    fileStream.close();
    return matrix;
}

}
